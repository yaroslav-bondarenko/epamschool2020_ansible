#!/bin/env bash

# Add records to /etc/hosts
echo -e '192.168.56.110 control.example.com control\n192.168.56.111 node1.example.com node1\n192.168.56.112 node2.example.com node2' >> /etc/hosts
# Allow Authentication by password and restart sshd
sed -i 's/PasswordAuthentication no/PasswordAuthentication yes/g' /etc/ssh/sshd_config && systemctl restart sshd
# Add new user "ansible" with password "password"
##useradd ansible
##echo ansible | passwd --stdin ansible
##echo "ansible ALL=(ALL) NOPASSWD:ALL" > /etc/sudoers.d/ansible
# Install packages
#yum update -y
yum install -y epel-release
yum install -y sshpass
#yum install -y git
#yum install -y ansible
#yum install -y vim
#yum install -y tree 





